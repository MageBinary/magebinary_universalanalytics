<?php

class Scommerce_UniversalAnalytics_Model_Source_Step
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 1, 'label'=>Mage::helper('adminhtml')->__('Billing information')),
            array('value' => 2, 'label'=>Mage::helper('adminhtml')->__('Shipping information')),
            array('value' => 3, 'label'=>Mage::helper('adminhtml')->__('Shipping method')),
            array('value' => 4, 'label'=>Mage::helper('adminhtml')->__('Payment information')),
            array('value' => 5, 'label'=>Mage::helper('adminhtml')->__('Order review')),
        );
    }
}