<?php
/**
 *
 * @category    Scommerce
 * @package     Scommerce_UniversalAnalytics
 * @author		Scommerce Mage (core@scommerce-mage.co.uk)
 */
class Scommerce_UniversalAnalytics_Block_List extends Mage_Catalog_Block_Product_List
{
    public function getProductCollection()
    {
       return $this->_getProductCollection();
    }
}