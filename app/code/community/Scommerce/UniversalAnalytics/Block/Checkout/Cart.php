<?php
/**
 *
 * @category    Scommerce
 * @package     Scommerce_UniversalAnalytics
 * @author		Scommerce Mage (core@scommerce-mage.co.uk)
 */
class Scommerce_UniversalAnalytics_Block_Checkout_Cart extends Mage_Core_Block_Template
{
    public function getItems()
    {
        return $this->getLayout()->getBlockSingleton('checkout/cart_crosssell')->getItems();
    }
}