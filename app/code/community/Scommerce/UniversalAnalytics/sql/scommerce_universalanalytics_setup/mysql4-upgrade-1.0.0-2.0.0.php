<?php
$installer = $this;

$installer->startSetup();

$sales_setup =  new Mage_Sales_Model_Mysql4_Setup('sales_setup');

$attribute  = array(
	'type'          => 'varchar',
	'label'         => 'Google Cookie',
	'default'       => '',
	'visible'       => false,
	'required'      => false,
	'user_defined'  => true,
	'searchable'    => false,
	'filterable'    => false,
	'comparable'    => false );

$installer->getConnection()->addColumn(
        $installer->getTable('sales_flat_order'),
        'google_cookie',
        'varchar(255) NULL DEFAULT NULL'
    );
	
$sales_setup->addAttribute('order', 'google_cookie', $attribute);

$installer->endSetup();