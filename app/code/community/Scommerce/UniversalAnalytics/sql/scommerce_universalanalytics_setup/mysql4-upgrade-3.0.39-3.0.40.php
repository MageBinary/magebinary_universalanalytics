<?php
$installer = $this;

$installer->startSetup();

$sales_setup =  new Mage_Sales_Model_Mysql4_Setup('sales_setup');

$attribute  = array(
	'type'          => 'varchar',
	'label'         => 'Google Category',
	'default'       => '',
	'visible'       => false,
	'required'      => false,
	'user_defined'  => true,
	'searchable'    => false,
	'filterable'    => false,
	'comparable'    => false );

$installer->getConnection()->addColumn(
        $installer->getTable('sales_flat_order_item'),
        'google_category',
        'varchar(255) NULL DEFAULT NULL'
    );

$installer->getConnection()->addColumn(
        $installer->getTable('sales_flat_quote_item'),
        'google_category',
        'varchar(255) NULL DEFAULT NULL'
    );	
	
$sales_setup->addAttribute('quote_item', 'google_category', $attribute);
$sales_setup->addAttribute('order_item', 'google_category', $attribute);

$installer->endSetup();